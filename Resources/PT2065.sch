EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "PT2065 - Encoder PCB"
Date "2020-11-12"
Rev "C"
Comp "TAWI"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_02x07_Counter_Clockwise J2
U 1 1 5FAC221B
P 9550 4500
F 0 "J2" H 9600 5017 50  0000 C CNN
F 1 "iDrive" H 9600 4926 50  0000 C CNN
F 2 "Connector_Molex:Molex_Mini-Fit_Jr_5566-14A_2x07_P4.20mm_Vertical" H 9550 4500 50  0001 C CNN
F 3 "~" H 9550 4500 50  0001 C CNN
	1    9550 4500
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x07_Counter_Clockwise J3
U 1 1 5FAC2999
P 9550 5600
F 0 "J3" H 9600 6117 50  0000 C CNN
F 1 "Motherboard" H 9600 6026 50  0000 C CNN
F 2 "Connector_Molex:Molex_Mini-Fit_Jr_5566-14A_2x07_P4.20mm_Vertical" H 9550 5600 50  0001 C CNN
F 3 "~" H 9550 5600 50  0001 C CNN
	1    9550 5600
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x07_Odd_Even J1
U 1 1 5FACB514
P 9550 3450
F 0 "J1" H 9600 3967 50  0000 C CNN
F 1 "Position Handcontroller" H 9600 3876 50  0000 C CNN
F 2 "TE_Connectivity:TE_1-188275-4" H 9550 3450 50  0001 C CNN
F 3 "~" H 9550 3450 50  0001 C CNN
	1    9550 3450
	1    0    0    -1  
$EndComp
$Comp
L Connector:Screw_Terminal_01x04 J6
U 1 1 5FACE119
P 10200 1200
F 0 "J6" H 10280 1192 50  0000 L CNN
F 1 "Encoder" H 10280 1101 50  0000 L CNN
F 2 "TerminalBlock_Phoenix:TerminalBlock_Phoenix_MKDS-1,5-4_1x04_P5.00mm_Horizontal" H 10200 1200 50  0001 C CNN
F 3 "~" H 10200 1200 50  0001 C CNN
	1    10200 1200
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 5FAD11B5
P 8900 4200
F 0 "R1" V 9000 4150 50  0000 L CNN
F 1 "10k" V 8900 4150 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.30x1.75mm_HandSolder" V 8830 4200 50  0001 C CNN
F 3 "~" H 8900 4200 50  0001 C CNN
	1    8900 4200
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R2
U 1 1 5FAD27E4
P 3850 5450
F 0 "R2" V 3950 5450 50  0000 C CNN
F 1 "10k" V 3850 5450 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.30x1.75mm_HandSolder" V 3780 5450 50  0001 C CNN
F 3 "~" H 3850 5450 50  0001 C CNN
	1    3850 5450
	0    -1   -1   0   
$EndComp
$Comp
L Regulator_Linear:L7805 U1
U 1 1 5FAD32EA
P 7300 950
F 0 "U1" H 7300 1192 50  0000 C CNN
F 1 "L7805" H 7300 1101 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:TO-263-2" H 7325 800 50  0001 L CIN
F 3 "http://www.st.com/content/ccc/resource/technical/document/datasheet/41/4f/b3/b0/12/d4/47/88/CD00000444.pdf/files/CD00000444.pdf/jcr:content/translations/en.CD00000444.pdf" H 7300 900 50  0001 C CNN
	1    7300 950 
	1    0    0    -1  
$EndComp
Text GLabel 10000 1300 0    50   Input ~ 0
Encoder_A_Phase
Text GLabel 10000 1400 0    50   Input ~ 0
Encoder_B_Phase
Wire Wire Line
	9350 4200 9050 4200
Wire Wire Line
	8750 4200 8500 4200
$Comp
L power:GND #PWR0101
U 1 1 5FAF0251
P 7300 1500
F 0 "#PWR0101" H 7300 1250 50  0001 C CNN
F 1 "GND" H 7305 1327 50  0000 C CNN
F 2 "" H 7300 1500 50  0001 C CNN
F 3 "" H 7300 1500 50  0001 C CNN
	1    7300 1500
	1    0    0    -1  
$EndComp
$Comp
L power:+24V #PWR0102
U 1 1 5FAF209D
P 6700 950
F 0 "#PWR0102" H 6700 800 50  0001 C CNN
F 1 "+24V" H 6715 1123 50  0000 C CNN
F 2 "" H 6700 950 50  0001 C CNN
F 3 "" H 6700 950 50  0001 C CNN
	1    6700 950 
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0103
U 1 1 5FAF2446
P 7900 950
F 0 "#PWR0103" H 7900 800 50  0001 C CNN
F 1 "+5V" H 7915 1123 50  0000 C CNN
F 2 "" H 7900 950 50  0001 C CNN
F 3 "" H 7900 950 50  0001 C CNN
	1    7900 950 
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0104
U 1 1 5FAF32F3
P 9400 1100
F 0 "#PWR0104" H 9400 950 50  0001 C CNN
F 1 "+5V" H 9415 1273 50  0000 C CNN
F 2 "" H 9400 1100 50  0001 C CNN
F 3 "" H 9400 1100 50  0001 C CNN
	1    9400 1100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0105
U 1 1 5FAF3869
P 9100 1200
F 0 "#PWR0105" H 9100 950 50  0001 C CNN
F 1 "GND" H 9105 1027 50  0000 C CNN
F 2 "" H 9100 1200 50  0001 C CNN
F 3 "" H 9100 1200 50  0001 C CNN
	1    9100 1200
	1    0    0    -1  
$EndComp
Wire Wire Line
	10000 1100 9400 1100
Text GLabel 9350 3250 0    50   Input ~ 0
Row1
Text GLabel 9850 3250 2    50   Input ~ 0
Row2
Text GLabel 9350 3350 0    50   Input ~ 0
Column1
Text GLabel 9850 3350 2    50   Input ~ 0
Column2
Text GLabel 9350 3450 0    50   Input ~ 0
Column3
Text GLabel 9850 3450 2    50   Input ~ 0
Column4
Text GLabel 9350 3550 0    50   Input ~ 0
Column5(MOSI-OC2)
Text GLabel 9850 3150 2    50   Input ~ 0
Indicator(SCK)
Text GLabel 9350 3750 0    50   Input ~ 0
KeySwitch(MISO)
$Comp
L power:GND #PWR0112
U 1 1 5FB14C1D
P 10200 3750
F 0 "#PWR0112" H 10200 3500 50  0001 C CNN
F 1 "GND" H 10205 3577 50  0000 C CNN
F 2 "" H 10200 3750 50  0001 C CNN
F 3 "" H 10200 3750 50  0001 C CNN
	1    10200 3750
	1    0    0    -1  
$EndComp
Text GLabel 9350 5400 0    50   Input ~ 0
Up_Handcontroller
NoConn ~ 9350 5500
NoConn ~ 9850 5800
NoConn ~ 9850 5600
NoConn ~ 9850 5500
Text GLabel 9850 5900 2    50   Input ~ 0
Down_Handcontroller
Text GLabel 9350 5600 0    50   Input ~ 0
Fast-Slow_Switch
Text GLabel 9350 5700 0    50   Input ~ 0
On-Off_Switch
Text GLabel 9350 4600 0    50   Input ~ 0
On-Off_Switch
Text GLabel 9350 5800 0    50   Input ~ 0
Inhibit_1
Text GLabel 9350 4700 0    50   Input ~ 0
Inhibit_1
Text GLabel 9850 5700 2    50   Input ~ 0
Status_Indicator
Text GLabel 9850 4600 2    50   Input ~ 0
Status_Indicator
NoConn ~ 9850 4500
NoConn ~ 9850 4700
NoConn ~ 9850 4800
NoConn ~ 9350 4400
NoConn ~ 9350 4300
$Comp
L power:+24V #PWR0113
U 1 1 5FB1EA17
P 9050 5050
F 0 "#PWR0113" H 9050 4900 50  0001 C CNN
F 1 "+24V" H 9065 5223 50  0000 C CNN
F 2 "" H 9050 5050 50  0001 C CNN
F 3 "" H 9050 5050 50  0001 C CNN
	1    9050 5050
	1    0    0    -1  
$EndComp
$Comp
L power:+24V #PWR0114
U 1 1 5FB1EFC6
P 9050 6150
F 0 "#PWR0114" H 9050 6000 50  0001 C CNN
F 1 "+24V" H 9065 6323 50  0000 C CNN
F 2 "" H 9050 6150 50  0001 C CNN
F 3 "" H 9050 6150 50  0001 C CNN
	1    9050 6150
	1    0    0    -1  
$EndComp
Wire Wire Line
	9350 5900 9350 6150
Wire Wire Line
	9350 6150 9050 6150
$Comp
L power:GND #PWR0115
U 1 1 5FB20111
P 8100 5300
F 0 "#PWR0115" H 8100 5050 50  0001 C CNN
F 1 "GND" H 8105 5127 50  0000 C CNN
F 2 "" H 8100 5300 50  0001 C CNN
F 3 "" H 8100 5300 50  0001 C CNN
	1    8100 5300
	1    0    0    -1  
$EndComp
Text GLabel 9850 5300 2    50   Input ~ 0
Limit_Switch_Lower_Mast
$Comp
L power:GND #PWR0116
U 1 1 5FB213A1
P 10650 4300
F 0 "#PWR0116" H 10650 4050 50  0001 C CNN
F 1 "GND" H 10655 4127 50  0000 C CNN
F 2 "" H 10650 4300 50  0001 C CNN
F 3 "" H 10650 4300 50  0001 C CNN
	1    10650 4300
	1    0    0    -1  
$EndComp
Wire Wire Line
	9850 4200 10000 4200
Wire Wire Line
	10000 4200 10000 4300
Wire Wire Line
	9850 4300 10000 4300
$Comp
L Transistor_BJT:BC337 Q1
U 1 1 5FB2446A
P 4550 5450
F 0 "Q1" H 4741 5496 50  0000 L CNN
F 1 "BC337" H 4741 5405 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-223" H 4750 5375 50  0001 L CIN
F 3 "https://diotec.com/tl_files/diotec/files/pdf/datasheets/bc337.pdf" H 4550 5450 50  0001 L CNN
	1    4550 5450
	1    0    0    -1  
$EndComp
Wire Wire Line
	4650 5000 4650 5250
$Comp
L power:GND #PWR0117
U 1 1 5FB28466
P 4650 5800
F 0 "#PWR0117" H 4650 5550 50  0001 C CNN
F 1 "GND" H 4655 5627 50  0000 C CNN
F 2 "" H 4650 5800 50  0001 C CNN
F 3 "" H 4650 5800 50  0001 C CNN
	1    4650 5800
	1    0    0    -1  
$EndComp
Text GLabel 3550 5450 0    50   Input ~ 0
Transistor_Reverse_Switch
$Comp
L Device:C C1
U 1 1 5FC28A4D
P 6800 1200
F 0 "C1" H 6915 1246 50  0000 L CNN
F 1 "0,33 uF" H 6915 1155 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.33x1.80mm_HandSolder" H 6838 1050 50  0001 C CNN
F 3 "~" H 6800 1200 50  0001 C CNN
	1    6800 1200
	1    0    0    -1  
$EndComp
$Comp
L Device:C C2
U 1 1 5FC273B0
P 7800 1200
F 0 "C2" H 7915 1246 50  0000 L CNN
F 1 "0,1uF" H 7915 1155 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.33x1.80mm_HandSolder" H 7838 1050 50  0001 C CNN
F 3 "~" H 7800 1200 50  0001 C CNN
	1    7800 1200
	1    0    0    -1  
$EndComp
Wire Wire Line
	7800 1350 7300 1350
Connection ~ 7300 1350
Wire Wire Line
	7300 1350 7300 1500
Wire Wire Line
	6800 1350 7300 1350
Wire Wire Line
	7900 950  7800 950 
Wire Wire Line
	6800 1050 6800 950 
Wire Wire Line
	7800 1050 7800 950 
Connection ~ 7800 950 
Wire Wire Line
	7800 950  7600 950 
Text GLabel 3650 2900 2    50   Input ~ 0
Reset
Text GLabel 5450 1900 2    50   Input ~ 0
Indicator(SCK)
Text GLabel 3650 1800 2    50   Input ~ 0
KeySwitch(MISO)
Text GLabel 3650 1500 2    50   Input ~ 0
Column3
Text GLabel 3650 2600 2    50   Input ~ 0
Column2
Text GLabel 3650 2500 2    50   Input ~ 0
Column1
Text GLabel 3650 2400 2    50   Input ~ 0
Row2
Text GLabel 3650 2300 2    50   Input ~ 0
Row1
Text GLabel 3650 3800 2    50   Input ~ 0
Limit_Switch_Lower_Mast
Text GLabel 3650 3500 2    50   Input ~ 0
Fast-Slow_Switch
Text GLabel 3650 3400 2    50   Input ~ 0
Encoder_B_Phase
Text GLabel 3650 3300 2    50   Input ~ 0
Encoder_A_Phase
$Comp
L Device:Crystal_GND2 Y1
U 1 1 5FB9AB43
P 4750 2400
F 0 "Y1" H 4750 2668 50  0000 C CNN
F 1 "Crystal_GND2" H 4750 2577 50  0000 C CNN
F 2 "Oscillator:Oscillator_SMD_SeikoEpson_SG8002CE-4Pin_3.2x2.5mm_HandSoldering" H 4750 2400 50  0001 C CNN
F 3 "~" H 4750 2400 50  0001 C CNN
	1    4750 2400
	1    0    0    -1  
$EndComp
Wire Wire Line
	4600 2400 4600 2100
Wire Wire Line
	4900 2000 4900 2400
$Comp
L power:GND #PWR05
U 1 1 5FB9FA30
P 4750 2600
F 0 "#PWR05" H 4750 2350 50  0001 C CNN
F 1 "GND" H 4755 2427 50  0000 C CNN
F 2 "" H 4750 2600 50  0001 C CNN
F 3 "" H 4750 2600 50  0001 C CNN
	1    4750 2600
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR04
U 1 1 5FBA0345
P 3050 4150
F 0 "#PWR04" H 3050 3900 50  0001 C CNN
F 1 "GND" H 3055 3977 50  0000 C CNN
F 2 "" H 3050 4150 50  0001 C CNN
F 3 "" H 3050 4150 50  0001 C CNN
	1    3050 4150
	1    0    0    -1  
$EndComp
$Comp
L Device:C C3
U 1 1 5FBA0720
P 1200 1650
F 0 "C3" H 1315 1696 50  0000 L CNN
F 1 "0,1uF" H 1315 1605 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.33x1.80mm_HandSolder" H 1238 1500 50  0001 C CNN
F 3 "~" H 1200 1650 50  0001 C CNN
	1    1200 1650
	1    0    0    -1  
$EndComp
Wire Wire Line
	1200 1400 1200 1500
$Comp
L power:GND #PWR01
U 1 1 5FBA3EEB
P 1200 1850
F 0 "#PWR01" H 1200 1600 50  0001 C CNN
F 1 "GND" H 1205 1677 50  0000 C CNN
F 2 "" H 1200 1850 50  0001 C CNN
F 3 "" H 1200 1850 50  0001 C CNN
	1    1200 1850
	1    0    0    -1  
$EndComp
$Comp
L Device:C C4
U 1 1 5FBA4297
P 4800 1250
F 0 "C4" H 4915 1296 50  0000 L CNN
F 1 "0,1uF" H 4915 1205 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.33x1.80mm_HandSolder" H 4838 1100 50  0001 C CNN
F 3 "~" H 4800 1250 50  0001 C CNN
	1    4800 1250
	1    0    0    -1  
$EndComp
$Comp
L Device:C C6
U 1 1 5FBA4653
P 6150 1250
F 0 "C6" H 6265 1296 50  0000 L CNN
F 1 "0,1uF" H 6265 1205 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.33x1.80mm_HandSolder" H 6188 1100 50  0001 C CNN
F 3 "~" H 6150 1250 50  0001 C CNN
	1    6150 1250
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C5
U 1 1 5FBA5AAC
P 5400 1250
F 0 "C5" H 5518 1296 50  0000 L CNN
F 1 "4,7 uF _16V" H 5518 1205 50  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-3216-12_Kemet-S_Pad1.58x1.35mm_HandSolder" H 5438 1100 50  0001 C CNN
F 3 "~" H 5400 1250 50  0001 C CNN
	1    5400 1250
	1    0    0    -1  
$EndComp
Wire Wire Line
	6150 1400 5400 1400
Connection ~ 5400 1400
Wire Wire Line
	5400 1400 4800 1400
$Comp
L power:GND #PWR08
U 1 1 5FBA8682
P 5400 1500
F 0 "#PWR08" H 5400 1250 50  0001 C CNN
F 1 "GND" H 5405 1327 50  0000 C CNN
F 2 "" H 5400 1500 50  0001 C CNN
F 3 "" H 5400 1500 50  0001 C CNN
	1    5400 1500
	1    0    0    -1  
$EndComp
Wire Wire Line
	6150 1100 5400 1100
Wire Wire Line
	3150 1100 3050 1100
Connection ~ 5400 1100
Wire Wire Line
	5400 1100 4800 1100
$Comp
L power:+5V #PWR07
U 1 1 5FBAE6C4
P 5400 1100
F 0 "#PWR07" H 5400 950 50  0001 C CNN
F 1 "+5V" H 5415 1273 50  0000 C CNN
F 2 "" H 5400 1100 50  0001 C CNN
F 3 "" H 5400 1100 50  0001 C CNN
	1    5400 1100
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_DPST_x2 SW1
U 1 1 5FBAFD2B
P 6250 5600
F 0 "SW1" H 6250 5885 50  0000 C CNN
F 1 "Reset" H 6250 5794 50  0000 C CNN
F 2 "Button_Switch_SMD:SW_Push_SPST_NO_Alps_SKRK" H 6250 5800 50  0001 C CNN
F 3 "http://www.apem.com/int/index.php?controller=attachment&id_attachment=488" H 6250 5800 50  0001 C CNN
	1    6250 5600
	1    0    0    -1  
$EndComp
$Comp
L Device:R R6
U 1 1 5FBB07DB
P 6650 5400
F 0 "R6" V 6750 5350 50  0000 L CNN
F 1 "10k" V 6650 5350 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.30x1.75mm_HandSolder" V 6580 5400 50  0001 C CNN
F 3 "~" H 6650 5400 50  0001 C CNN
	1    6650 5400
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR010
U 1 1 5FBBDA4A
P 6650 5250
F 0 "#PWR010" H 6650 5100 50  0001 C CNN
F 1 "+5V" H 6665 5423 50  0000 C CNN
F 2 "" H 6650 5250 50  0001 C CNN
F 3 "" H 6650 5250 50  0001 C CNN
	1    6650 5250
	1    0    0    -1  
$EndComp
Text GLabel 6850 5600 2    50   Input ~ 0
Reset
Wire Wire Line
	6450 5600 6650 5600
Wire Wire Line
	6650 5550 6650 5600
Connection ~ 6650 5600
Wire Wire Line
	6650 5600 6850 5600
$Comp
L power:GND #PWR09
U 1 1 5FBC6B24
P 5950 5700
F 0 "#PWR09" H 5950 5450 50  0001 C CNN
F 1 "GND" H 5955 5527 50  0000 C CNN
F 2 "" H 5950 5700 50  0001 C CNN
F 3 "" H 5950 5700 50  0001 C CNN
	1    5950 5700
	1    0    0    -1  
$EndComp
Wire Wire Line
	6050 5600 5950 5600
Wire Wire Line
	5950 5600 5950 5700
Wire Wire Line
	5450 1900 5300 1900
$Comp
L Device:R R5
U 1 1 5FBCA590
P 5300 2150
F 0 "R5" V 5400 2100 50  0000 L CNN
F 1 "85" V 5300 2100 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.30x1.75mm_HandSolder" V 5230 2150 50  0001 C CNN
F 3 "~" H 5300 2150 50  0001 C CNN
	1    5300 2150
	1    0    0    -1  
$EndComp
Wire Wire Line
	5300 2000 5300 1900
$Comp
L Device:LED D1
U 1 1 5FBCCA68
P 5300 2600
F 0 "D1" V 5339 2482 50  0000 R CNN
F 1 "Blue" V 5248 2482 50  0000 R CNN
F 2 "LED_SMD:LED_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 5300 2600 50  0001 C CNN
F 3 "~" H 5300 2600 50  0001 C CNN
	1    5300 2600
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR06
U 1 1 5FBCE602
P 5300 2850
F 0 "#PWR06" H 5300 2600 50  0001 C CNN
F 1 "GND" H 5305 2677 50  0000 C CNN
F 2 "" H 5300 2850 50  0001 C CNN
F 3 "" H 5300 2850 50  0001 C CNN
	1    5300 2850
	1    0    0    -1  
$EndComp
Wire Wire Line
	5300 2750 5300 2850
Wire Wire Line
	5300 2450 5300 2300
$Comp
L Device:LED D2
U 1 1 5FBD9BD0
P 8450 1150
F 0 "D2" V 8489 1032 50  0000 R CNN
F 1 "Green" V 8398 1032 50  0000 R CNN
F 2 "LED_SMD:LED_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 8450 1150 50  0001 C CNN
F 3 "~" H 8450 1150 50  0001 C CNN
	1    8450 1150
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R8
U 1 1 5FBDA4A2
P 8200 950
F 0 "R8" V 8300 900 50  0000 L CNN
F 1 "140" V 8200 900 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.30x1.75mm_HandSolder" V 8130 950 50  0001 C CNN
F 3 "~" H 8200 950 50  0001 C CNN
	1    8200 950 
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7900 950  8050 950 
Connection ~ 7900 950 
Wire Wire Line
	8350 950  8450 950 
Wire Wire Line
	8450 950  8450 1000
$Comp
L power:GND #PWR011
U 1 1 5FBDEAF7
P 8450 1500
F 0 "#PWR011" H 8450 1250 50  0001 C CNN
F 1 "GND" H 8455 1327 50  0000 C CNN
F 2 "" H 8450 1500 50  0001 C CNN
F 3 "" H 8450 1500 50  0001 C CNN
	1    8450 1500
	1    0    0    -1  
$EndComp
Wire Wire Line
	8450 1300 8450 1500
$Comp
L power:+24V #PWR012
U 1 1 60B62E6B
P 8850 3150
F 0 "#PWR012" H 8850 3000 50  0001 C CNN
F 1 "+24V" H 8865 3323 50  0000 C CNN
F 2 "" H 8850 3150 50  0001 C CNN
F 3 "" H 8850 3150 50  0001 C CNN
	1    8850 3150
	1    0    0    -1  
$EndComp
$Comp
L MCU_Microchip_ATmega:ATmega328P-AU U2
U 1 1 5FB65334
P 3050 2600
F 0 "U2" H 3050 800 50  0000 C CNN
F 1 "ATmega328P-AU" H 3050 700 50  0000 C CNN
F 2 "Package_QFP:TQFP-32_7x7mm_P0.8mm" H 3050 2600 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/ATmega328_P%20AVR%20MCU%20with%20picoPower%20Technology%20Data%20Sheet%2040001984A.pdf" H 3050 2600 50  0001 C CNN
	1    3050 2600
	1    0    0    -1  
$EndComp
Wire Wire Line
	4800 1100 3150 1100
Connection ~ 4800 1100
Connection ~ 3150 1100
Text GLabel 3650 1700 2    50   Input ~ 0
Column5(MOSI-OC2)
Text GLabel 3650 1600 2    50   Input ~ 0
Column4
$Comp
L Connector:AVR-ISP-6 J4
U 1 1 60B8868D
P 1600 6750
F 0 "J4" H 1271 6846 50  0000 R CNN
F 1 "AVR-ISP-6" H 1271 6755 50  0000 R CNN
F 2 "Connector:Tag-Connect_TC2030-IDC-NL_2x03_P1.27mm_Vertical" V 1350 6800 50  0001 C CNN
F 3 " ~" H 325 6200 50  0001 C CNN
	1    1600 6750
	1    0    0    -1  
$EndComp
Text GLabel 2000 6550 2    50   Input ~ 0
KeySwitch(MISO)
Text GLabel 2000 6650 2    50   Input ~ 0
Column5(MOSI-OC2)
Text GLabel 2000 6850 2    50   Input ~ 0
Reset
Text GLabel 2000 6750 2    50   Input ~ 0
Indicator(SCK)
$Comp
L power:GND #PWR03
U 1 1 60B89909
P 1500 7200
F 0 "#PWR03" H 1500 6950 50  0001 C CNN
F 1 "GND" H 1505 7027 50  0000 C CNN
F 2 "" H 1500 7200 50  0001 C CNN
F 3 "" H 1500 7200 50  0001 C CNN
	1    1500 7200
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR02
U 1 1 60B89E84
P 1500 6250
F 0 "#PWR02" H 1500 6100 50  0001 C CNN
F 1 "+5V" H 1515 6423 50  0000 C CNN
F 2 "" H 1500 6250 50  0001 C CNN
F 3 "" H 1500 6250 50  0001 C CNN
	1    1500 6250
	1    0    0    -1  
$EndComp
Wire Wire Line
	4650 5650 4650 5700
Wire Wire Line
	4350 5450 4150 5450
$Comp
L Device:R R7
U 1 1 60B9CB5C
P 4300 5700
F 0 "R7" V 4400 5700 50  0000 C CNN
F 1 "8k" V 4300 5700 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.30x1.75mm_HandSolder" V 4230 5700 50  0001 C CNN
F 3 "~" H 4300 5700 50  0001 C CNN
	1    4300 5700
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4450 5700 4650 5700
Wire Wire Line
	4150 5700 4150 5450
Connection ~ 4150 5450
Wire Wire Line
	4150 5450 4000 5450
Wire Wire Line
	5400 1400 5400 1500
Wire Wire Line
	7300 1350 7300 1250
Wire Wire Line
	3050 4100 3050 4150
Wire Wire Line
	9350 4800 9350 5050
Wire Wire Line
	9350 5050 9050 5050
Wire Wire Line
	1500 7150 1500 7200
Wire Wire Line
	1200 1800 1200 1850
Wire Wire Line
	4650 5700 4650 5800
Connection ~ 4650 5700
$Comp
L Analog_DAC:MCP4725xxx-xCH U3
U 1 1 60D2074D
P 3850 6650
F 0 "U3" H 4000 6250 50  0000 L CNN
F 1 "MCP4725xxx-xCH" H 4000 6350 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23-6" H 3850 6400 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/22039d.pdf" H 3850 6650 50  0001 C CNN
	1    3850 6650
	1    0    0    -1  
$EndComp
Text GLabel 3450 6550 0    50   Input ~ 0
SCL
Text GLabel 3450 6650 0    50   Input ~ 0
SDA
$Comp
L power:+5V #PWR0106
U 1 1 60D221C6
P 3850 6200
F 0 "#PWR0106" H 3850 6050 50  0001 C CNN
F 1 "+5V" H 3865 6373 50  0000 C CNN
F 2 "" H 3850 6200 50  0001 C CNN
F 3 "" H 3850 6200 50  0001 C CNN
	1    3850 6200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0107
U 1 1 60D22726
P 3850 7100
F 0 "#PWR0107" H 3850 6850 50  0001 C CNN
F 1 "GND" H 3855 6927 50  0000 C CNN
F 2 "" H 3850 7100 50  0001 C CNN
F 3 "" H 3850 7100 50  0001 C CNN
	1    3850 7100
	1    0    0    -1  
$EndComp
Text GLabel 3650 2800 2    50   Input ~ 0
SCL
Text GLabel 3650 2700 2    50   Input ~ 0
SDA
Wire Wire Line
	3850 6350 3850 6200
Text GLabel 9850 4400 2    50   Input ~ 0
Reverse_Switch
Text GLabel 4250 6650 2    50   Input ~ 0
Trottle_Wiper
Text GLabel 8500 4200 0    50   Input ~ 0
Trottle_Wiper
Wire Wire Line
	10650 4300 10000 4300
Connection ~ 10000 4300
Text GLabel 3650 1400 2    50   Input ~ 0
Transistor_Reverse_Switch
Wire Wire Line
	9350 5300 8100 5300
$Comp
L power:GND #PWR0108
U 1 1 60D41D72
P 10750 5400
F 0 "#PWR0108" H 10750 5150 50  0001 C CNN
F 1 "GND" H 10755 5227 50  0000 C CNN
F 2 "" H 10750 5400 50  0001 C CNN
F 3 "" H 10750 5400 50  0001 C CNN
	1    10750 5400
	1    0    0    -1  
$EndComp
Wire Wire Line
	10750 5400 9850 5400
Wire Wire Line
	10000 1200 9100 1200
Wire Wire Line
	9350 3150 8850 3150
Text GLabel 4650 5000 2    50   Input ~ 0
Reverse_Switch
NoConn ~ 9350 4500
Text Notes 4400 3550 0    50   ~ 0
Fast/Slow on handcontroller
Text Notes 4650 3850 0    50   ~ 0
Lower limitswitch mounted on mast
Wire Wire Line
	4900 2000 3650 2000
Wire Wire Line
	3650 2100 4600 2100
Wire Wire Line
	5300 1900 3650 1900
Connection ~ 5300 1900
Text GLabel 2450 1600 0    50   Input ~ 0
Up_Handcontroller
Text GLabel 2450 1700 0    50   Input ~ 0
Down_Handcontroller
Wire Wire Line
	2450 1400 1200 1400
Text Notes 4100 3200 0    50   ~ 0
TX
Text Notes 4100 3100 0    50   ~ 0
RX
NoConn ~ 9350 3650
Wire Wire Line
	10200 3750 9850 3750
NoConn ~ 9850 3650
Wire Wire Line
	3450 6750 3300 6750
Wire Wire Line
	3300 6750 3300 7000
Wire Wire Line
	3300 7000 3850 7000
Wire Wire Line
	3700 5450 3550 5450
Wire Wire Line
	6700 950  6800 950 
Connection ~ 6800 950 
Wire Wire Line
	6800 950  7000 950 
Wire Wire Line
	3850 6950 3850 7000
Connection ~ 3850 7000
Wire Wire Line
	3850 7000 3850 7100
$Comp
L power:+5V #PWR0109
U 1 1 61C3C172
P 10400 3550
F 0 "#PWR0109" H 10400 3400 50  0001 C CNN
F 1 "+5V" H 10415 3723 50  0000 C CNN
F 2 "" H 10400 3550 50  0001 C CNN
F 3 "" H 10400 3550 50  0001 C CNN
	1    10400 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	10400 3550 9850 3550
$Comp
L Connector:Screw_Terminal_01x06 J?
U 1 1 61C41891
P 10200 2200
F 0 "J?" H 10280 2192 50  0000 L CNN
F 1 "AUX" H 10280 2101 50  0000 L CNN
F 2 "TerminalBlock_Phoenix:TerminalBlock_Phoenix_MKDS-1,5-4_1x04_P5.00mm_Horizontal" H 10200 2200 50  0001 C CNN
F 3 "~" H 10200 2200 50  0001 C CNN
	1    10200 2200
	1    0    0    -1  
$EndComp
Text GLabel 3650 3100 2    50   Input ~ 0
PD0(RX)
Text GLabel 3650 3200 2    50   Input ~ 0
PD1(TX)
Text GLabel 3650 3600 2    50   Input ~ 0
PD5(pwm)
Text GLabel 3650 3700 2    50   Input ~ 0
PD6(pwm)
Text GLabel 10000 2300 0    50   Input ~ 0
PD0(RX)
Text GLabel 10000 2200 0    50   Input ~ 0
PD1(TX)
Text GLabel 10000 2400 0    50   Input ~ 0
PD5(pwm)
Text GLabel 10000 2500 0    50   Input ~ 0
PD6(pwm)
$Comp
L power:+5V #PWR?
U 1 1 61C44FEE
P 9400 2000
F 0 "#PWR?" H 9400 1850 50  0001 C CNN
F 1 "+5V" H 9415 2173 50  0000 C CNN
F 2 "" H 9400 2000 50  0001 C CNN
F 3 "" H 9400 2000 50  0001 C CNN
	1    9400 2000
	1    0    0    -1  
$EndComp
Wire Wire Line
	10000 2000 9400 2000
$Comp
L power:GND #PWR?
U 1 1 61C47400
P 9100 2100
F 0 "#PWR?" H 9100 1850 50  0001 C CNN
F 1 "GND" H 9105 1927 50  0000 C CNN
F 2 "" H 9100 2100 50  0001 C CNN
F 3 "" H 9100 2100 50  0001 C CNN
	1    9100 2100
	1    0    0    -1  
$EndComp
Wire Wire Line
	10000 2100 9100 2100
$EndSCHEMATC
